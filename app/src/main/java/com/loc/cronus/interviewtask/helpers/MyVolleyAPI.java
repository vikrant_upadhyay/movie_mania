package com.loc.cronus.interviewtask.helpers;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * Created by Cronus on 7/3/2018.
 */

public class MyVolleyAPI {

    private static MyVolleyAPI mINnstance;
    private RequestQueue requestQueue;
    private static Context mCtx;


    private MyVolleyAPI(Context context) {
        mCtx = context;
        requestQueue = getRequestQueue();
    }

    public static synchronized MyVolleyAPI getInstance(Context context) {
        if (mINnstance == null) {
            mINnstance = new MyVolleyAPI(context);
        }
        return mINnstance;
    }

    public RequestQueue getRequestQueue() {
        if (requestQueue == null) {
            requestQueue = Volley.newRequestQueue(mCtx.getApplicationContext());
        }
        return requestQueue;
    }

    public <T> void addTorequestque(Request<T> request) {
        requestQueue.add(request);
    }
}
