package com.loc.cronus.interviewtask.dataobjects;

import java.io.Serializable;

/**
 * Created by Cronus on 7/3/2018.
 */

public class MovieRatingDataObject implements Serializable {

    private String Source;
    private String Value;

    public MovieRatingDataObject(String source, String value) {
        Source = source;
        Value = value;
    }

    public String getSource() {
        return Source;
    }

    public void setSource(String source) {
        Source = source;
    }

    public String getValue() {
        return Value;
    }

    public void setValue(String value) {
        Value = value;
    }
}
