package com.loc.cronus.interviewtask.activities;

import android.app.Application;

import com.loc.cronus.interviewtask.helpers.InternetConnectivityCheck;

/**
 * Created by Cronus on 7/4/2018.
 */
public class ConnectionActivity extends Application {

    private static ConnectionActivity mInstance;

    @Override
    public void onCreate() {
        super.onCreate();

        mInstance = this;
    }

    public static synchronized ConnectionActivity getInstance() {
        return mInstance;
    }

    public void setConnectivityListener(InternetConnectivityCheck.ConnectivityReceiverListener listener) {
        InternetConnectivityCheck.connectivityReceiverListener = listener;
    }
}