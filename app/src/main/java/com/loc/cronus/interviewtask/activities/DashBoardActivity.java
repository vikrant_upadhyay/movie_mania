package com.loc.cronus.interviewtask.activities;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.graphics.Color;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.loc.cronus.interviewtask.R;
import com.loc.cronus.interviewtask.fragments.MovieListFragment;
import com.loc.cronus.interviewtask.helpers.InternetConnectivityCheck;

public class DashBoardActivity extends AppCompatActivity {

    RelativeLayout dashlayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dash_board);
        mappingFunction();
        fragmentAlocFunction();
    }

    private void mappingFunction() {
        dashlayout = (RelativeLayout) findViewById(R.id.dashlayout);
    }

    private void fragmentAlocFunction() {
        Fragment fragment = new MovieListFragment();
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container_for_fragment, fragment, "new").commit();
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkConnection();
    }

    private void checkConnection() {
        boolean isConnected = InternetConnectivityCheck.isConnected();
        showSnack(isConnected);
    }

    public void showSnack(boolean isConnected) {
        String message;
        int color;
        if (!isConnected) {
            message = "No connection";
            color = Color.WHITE;
            final Snackbar snackbar = Snackbar.make(dashlayout, message, Snackbar.LENGTH_INDEFINITE);
            snackbar.setAction("RETRY", new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (InternetConnectivityCheck.isConnected()) {
                        fragmentAlocFunction();
                    }
                }
            });
            View sbView = snackbar.getView();
            TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
            textView.setTextColor(color);
            snackbar.show();
        }
    }
}
