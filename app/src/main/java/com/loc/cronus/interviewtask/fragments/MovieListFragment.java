package com.loc.cronus.interviewtask.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.loc.cronus.interviewtask.R;
import com.loc.cronus.interviewtask.activities.DashBoardActivity;
import com.loc.cronus.interviewtask.adapters.MovieListAdapter;
import com.loc.cronus.interviewtask.dataobjects.MovieDataObject;
import com.loc.cronus.interviewtask.dataobjects.MovieRatingDataObject;

import com.loc.cronus.interviewtask.helpers.MyVolleyAPI;
import com.loc.cronus.interviewtask.helpers.Webservices;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Cronus on 7/3/2018.
 */

public class MovieListFragment extends Fragment {
    View view;
    private RecyclerView all_movies_recycler_view;
    MovieListAdapter movieListAdapter;
    List<MovieDataObject> movieDataObjects = new ArrayList<MovieDataObject>();
    private ProgressBar progress_bar_for_movie_list;
    ArrayList<MovieRatingDataObject> ratingDataObjects = new ArrayList<MovieRatingDataObject>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_movie_list_layout, container, false);
        ((DashBoardActivity) getActivity()).setTitle("Movies");

        mappingFunction();
        initFunction();

        return view;
    }

    @Override
    public void onResume() {
        movieDataObjects.clear();
        ratingDataObjects.clear();
        volleyForFetchingMovies();
        super.onResume();
    }

    private void mappingFunction() {
        all_movies_recycler_view = (RecyclerView) view.findViewById(R.id.all_movies_recycler_view);
        progress_bar_for_movie_list = (ProgressBar) view.findViewById(R.id.progress_bar_for_movie_list);
    }

    private void initFunction() {

    }

    private void volleyForFetchingMovies() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Webservices.MOVIE_LIST_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
//                Log.d("TAG RESPONSE :", response + "");
                try {
                    JSONObject object = new JSONObject(response);

                    if (Boolean.parseBoolean(object.getString("Response"))) {
                        JSONArray array = object.getJSONArray("Ratings");
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject obj = array.getJSONObject(i);
                            Log.d("TAG RATING :", obj.getString("Source") + " - " + obj.getString("Value"));
                            ratingDataObjects.add(new MovieRatingDataObject(obj.getString("Source"), obj.getString("Value")));
                        }

                        movieDataObjects.add(new MovieDataObject(object.getString("Title"), object.getString("Year"),
                                object.getString("Rated"), object.getString("Released"),
                                object.getString("Runtime"), object.getString("Genre"),
                                object.getString("Director"), object.getString("Writer"),
                                object.getString("Actors"), object.getString("Plot"),
                                object.getString("Language"), object.getString("Country"),
                                object.getString("Awards"), object.getString("Poster"),
                                ratingDataObjects, object.getString("Metascore"),
                                object.getString("imdbRating"), object.getString("imdbVotes"),
                                object.getString("imdbID"), object.getString("Type"),
                                object.getString("DVD"), object.getString("BoxOffice"),
                                object.getString("Production"), object.getString("Website")));

                        Log.d("TAG OBJECT:", movieDataObjects + "");

                        movieListAdapter = new MovieListAdapter(getActivity(), movieDataObjects);
                        RecyclerView.LayoutManager oLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                        all_movies_recycler_view.setLayoutManager(oLayoutManager);
                        all_movies_recycler_view.setAdapter(movieListAdapter);
                        progress_bar_for_movie_list.setVisibility(View.GONE);
                    } else {
                        Toast.makeText(getActivity(), "Something Went Wrong ! ", Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    Log.d("TAG ERROR", e + "");
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        stringRequest.setShouldCache(false);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(5000, 2, 1));
        MyVolleyAPI.getInstance(getActivity()).addTorequestque(stringRequest);
    }
}
