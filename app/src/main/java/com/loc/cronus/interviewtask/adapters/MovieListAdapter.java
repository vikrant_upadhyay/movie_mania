package com.loc.cronus.interviewtask.adapters;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.loc.cronus.interviewtask.R;
import com.loc.cronus.interviewtask.dataobjects.MovieDataObject;
import com.loc.cronus.interviewtask.fragments.MovieDescriptionFragment;
import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Cronus on 7/3/2018.
 */

public class MovieListAdapter extends RecyclerView.Adapter<MovieListAdapter.MovieAdapter> implements Serializable {

    Context context;
    List<MovieDataObject> movieListAdapters = new ArrayList<MovieDataObject>();

    public MovieListAdapter(Context context, List<MovieDataObject> movieListAdapters) {
        this.context = context;
        this.movieListAdapters = movieListAdapters;
    }

    @NonNull
    @Override
    public MovieListAdapter.MovieAdapter onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.movie_list_item_layout, parent, false);
        return new MovieListAdapter.MovieAdapter(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MovieListAdapter.MovieAdapter holder, int position) {
        final MovieDataObject movieDataObject = movieListAdapters.get(position);

        Picasso.with(context).load(movieDataObject.getPoster()).resize(630, 400).centerCrop().into(holder.poster_of_movie);

        holder.movie_name_field.setText(movieDataObject.getTitle());
        holder.directors_name_field.setText("Directed By : " + movieDataObject.getDirector());
        holder.linear_for_onClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new MovieDescriptionFragment();
                Bundle args = new Bundle();
                args.putString("Title", movieDataObject.getTitle());
                args.putString("Year", movieDataObject.getYear());
                args.putString("Rated", movieDataObject.getRated());
                args.putString("Released", movieDataObject.getReleased());
                args.putString("Runtime", movieDataObject.getRuntime());
                args.putString("Genre", movieDataObject.getGenre());
                args.putString("Director", movieDataObject.getDirector());
                args.putString("Writer", movieDataObject.getWriter());
                args.putString("Actors", movieDataObject.getActors());
                args.putString("Plot", movieDataObject.getPlot());
                args.putString("Language", movieDataObject.getLanguage());
                args.putString("Country", movieDataObject.getCountry());
                args.putString("Awards", movieDataObject.getAwards());
                args.putString("Poster", movieDataObject.getPoster());
                args.putSerializable("Ratings", movieDataObject.getRatings());
                args.putString("Metascore", movieDataObject.getMetascore());
                args.putString("imdbRating", movieDataObject.getImdbRating());
                args.putString("imdbVotes", movieDataObject.getImdbVotes());
                args.putString("imdbID", movieDataObject.getImdbID());
                args.putString("Type", movieDataObject.getType());
                args.putString("DVD", movieDataObject.getDVD());
                args.putString("BoxOffice", movieDataObject.getBoxOffice());
                args.putString("Production", movieDataObject.getProduction());
                args.putString("Website", movieDataObject.getWebsite());
                fragment.setArguments(args);
                FragmentManager fragmentManager = ((Activity) context).getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.container_for_fragment, fragment, "newt");
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });
    }

    @Override
    public int getItemCount() {
        return movieListAdapters.size();
    }

    public class MovieAdapter extends RecyclerView.ViewHolder {
        private ImageView poster_of_movie;
        private TextView movie_name_field;
        private TextView directors_name_field;
        private LinearLayout linear_for_onClick;


        public MovieAdapter(View itemView) {
            super(itemView);
            poster_of_movie = (ImageView) itemView.findViewById(R.id.poster_of_movie);
            directors_name_field = (TextView) itemView.findViewById(R.id.directors_name_field);
            movie_name_field = (TextView) itemView.findViewById(R.id.movie_name_field);
            linear_for_onClick = (LinearLayout) itemView.findViewById(R.id.linear_for_onClick);
        }
    }
}
