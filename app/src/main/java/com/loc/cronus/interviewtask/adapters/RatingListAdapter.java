package com.loc.cronus.interviewtask.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.loc.cronus.interviewtask.R;
import com.loc.cronus.interviewtask.dataobjects.MovieRatingDataObject;

import java.util.ArrayList;

/**
 * Created by Cronus on 7/4/2018.
 */

public class RatingListAdapter extends RecyclerView.Adapter<RatingListAdapter.RateAdapter> {

    Context context;
    ArrayList<MovieRatingDataObject> movieRatingDataObjects = new ArrayList<MovieRatingDataObject>();

    public RatingListAdapter(Context context, ArrayList<MovieRatingDataObject> movieRatingDataObjects) {
        this.context = context;
        this.movieRatingDataObjects = movieRatingDataObjects;
    }

    @NonNull
    @Override
    public RatingListAdapter.RateAdapter onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rating_list_item_layout, parent, false);
        return new RatingListAdapter.RateAdapter(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RatingListAdapter.RateAdapter holder, int position) {
        MovieRatingDataObject movieRatingDataObject = movieRatingDataObjects.get(position);
        holder.critic_name.setText(movieRatingDataObject.getSource());
        holder.rating_field_for_item.setText(movieRatingDataObject.getValue());
    }

    @Override
    public int getItemCount() {
        return movieRatingDataObjects.size();
    }

    public class RateAdapter extends RecyclerView.ViewHolder {
        private TextView critic_name, rating_field_for_item;

        public RateAdapter(View itemView) {
            super(itemView);
            critic_name = (TextView) itemView.findViewById(R.id.critic_name);
            rating_field_for_item = (TextView) itemView.findViewById(R.id.rating_field_for_item);
        }
    }
}
