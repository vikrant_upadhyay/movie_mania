package com.loc.cronus.interviewtask.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.util.Linkify;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.loc.cronus.interviewtask.R;
import com.loc.cronus.interviewtask.activities.DashBoardActivity;
import com.loc.cronus.interviewtask.adapters.RatingListAdapter;
import com.loc.cronus.interviewtask.dataobjects.MovieRatingDataObject;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Cronus on 7/3/2018.
 */

public class MovieDescriptionFragment extends Fragment {

    View view;
    private ImageView expanded_movie_Image;
    private TextView production_by_field, boxoffice_field, site_link_field, votes_of_movie, metascore_of_movie, crew_of_movie, title_of_movie, plot_of_movie, runtime_of_movie, country_of_movie, language_of_movie, actors_of_movie;
    String movieproductioncompany, movieboxoffice, movielink, movieVotes, movieMetascore, movieCrew, movieActors, picUrl, movieTitle, movieLang, movieRuntime, moviecountry, movieplot, movieGenre, movieRelease;
    ArrayList<MovieRatingDataObject> movieRatingDataObjects = new ArrayList<MovieRatingDataObject>();
    String buildstrig = null;
    RecyclerView recycler_view_for_ratings;
    RatingListAdapter ratingListAdapter;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_movie_description_layout, container, false);
        ((DashBoardActivity) getActivity()).setTitle("Movie Description");

        mappingFunction();
        initFunction();

        return view;
    }

    private void mappingFunction() {
        expanded_movie_Image = (ImageView) view.findViewById(R.id.expanded_movie_Image);
        title_of_movie = (TextView) view.findViewById(R.id.title_of_movie);
        plot_of_movie = (TextView) view.findViewById(R.id.plot_of_movie);
        runtime_of_movie = (TextView) view.findViewById(R.id.runtime_of_movie);
        country_of_movie = (TextView) view.findViewById(R.id.country_of_movie);
        language_of_movie = (TextView) view.findViewById(R.id.language_of_movie);
        actors_of_movie = (TextView) view.findViewById(R.id.actors_of_movie);
        crew_of_movie = (TextView) view.findViewById(R.id.crew_of_movie);
        metascore_of_movie = (TextView) view.findViewById(R.id.metascore_of_movie);
        votes_of_movie = (TextView) view.findViewById(R.id.votes_of_movie);
        production_by_field = (TextView) view.findViewById(R.id.production_by_field);
        boxoffice_field = (TextView) view.findViewById(R.id.boxoffice_field);
        site_link_field = (TextView) view.findViewById(R.id.site_link_field);
        recycler_view_for_ratings = (RecyclerView) view.findViewById(R.id.recycler_view_for_ratings);
    }

    private void initFunction() {

        Bundle bundle = getArguments();
        if (bundle != null) {
            bundle.get("Title");
            movieRatingDataObjects = (ArrayList<MovieRatingDataObject>) getArguments().getSerializable("Ratings");
            picUrl = (String) bundle.get("Poster");
            movieTitle = (String) bundle.get("Title");
            movieLang = (String) bundle.get("Language");
            moviecountry = (String) bundle.get("Country");
            movieplot = (String) bundle.get("Plot");
            movieRuntime = (String) bundle.get("Runtime");
            movieGenre = (String) bundle.get("Genre");
            movieRelease = (String) bundle.get("Released");
            movieActors = (String) bundle.get("Actors");
            movieCrew = (String) bundle.get("Writer");
            movieMetascore = (String) bundle.get("Metascore");
            movieVotes = (String) bundle.get("imdbVotes");
            movieproductioncompany = (String) bundle.get("Production");
            movieboxoffice = (String) bundle.get("BoxOffice");
            movielink = (String) bundle.get("Website");

            StringBuilder builder = new StringBuilder();
            if (movieTitle.trim().length() >= 22) {
//            Toast.makeText(context, "" + obj.getProductname().length(), Toast.LENGTH_SHORT).show();

                for (int i = 0; i <= movieTitle.length(); i++) {
                    if (i == 17) {
                        buildstrig = movieTitle.substring(0, i);
                        Log.e("NAME::# ", "" + buildstrig);
                        break;
                    }
                }
                builder.append(buildstrig + "...");
            } else {
                builder.append(movieTitle);
            }
            Picasso.with(getActivity()).load(picUrl).into(expanded_movie_Image);
            title_of_movie.setText(builder);
            language_of_movie.setText(movieLang);
            country_of_movie.setText(moviecountry + " | " + movieRelease);
            runtime_of_movie.setText(movieRuntime + " | " + movieGenre);
            plot_of_movie.setText(movieplot);
            actors_of_movie.setText(movieActors);
            crew_of_movie.setText(movieCrew);
            metascore_of_movie.setText(movieMetascore + " %");
            votes_of_movie.setText(movieVotes + " Votes");
            production_by_field.setText(movieproductioncompany);
            boxoffice_field.setText(movieboxoffice);
            site_link_field.setText(movielink);
            Linkify.addLinks(site_link_field, Linkify.ALL);

            ratingListAdapter = new RatingListAdapter(getActivity(), movieRatingDataObjects);
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
            recycler_view_for_ratings.setLayoutManager(layoutManager);
            recycler_view_for_ratings.setAdapter(ratingListAdapter);
        } else {
            Toast.makeText(getActivity(), "No Values For this movie", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }
}
